package no.uib.secmdcs.srpclient.crypto;

import java.math.BigInteger;

/**
 * Holds the values A and M1, generated after Round1 and to be sent to server at Round2
 * @author daniele */
public class SrpClientPublicValues {
	private BigInteger A;
	private BigInteger M1;
	
	public SrpClientPublicValues(BigInteger a, BigInteger m1) {
		super();
		A = a;
		M1 = m1;
	}
	public BigInteger getA() {
		return A;
	}
	public void setA(BigInteger a) {
		A = a;
	}
	public BigInteger getM1() {
		return M1;
	}
	public void setM1(BigInteger m1) {
		M1 = m1;
	}
}
