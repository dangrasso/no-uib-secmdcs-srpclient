package no.uib.secmdcs.srpclient.crypto.provider.sc;

import java.math.BigInteger;
import java.security.SecureRandom;

import no.uib.secmdcs.srpclient.crypto.SrpClientPublicValues;
import no.uib.secmdcs.srpclient.crypto.SrpConst;
import no.uib.secmdcs.srpclient.crypto.SrpException;
import no.uib.secmdcs.srpclient.crypto.provider.ISrpClientProvider;

import org.spongycastle.crypto.CryptoException;
import org.spongycastle.crypto.Digest;

/**
 * Provider for the client-side SRP authentication, based on SpongyCastle (SC)
 * @author daniele
 *
 */
public class SrpClientProviderSCAdapter implements ISrpClientProvider {

	private static final BigInteger N =SrpConst.SRP_DEFAULT_N;
	private static final BigInteger g =SrpConst.SRP_DEFAULT_G;
	private static final String H =SrpConst.SRP_DEFAULT_H;

	private SecureRandom random;
	private Digest digest;

	private SRP6ClientCustom client = null;	

	public SrpClientProviderSCAdapter() throws SrpException {
		random = new SecureRandom();
		digest = DigestFactorySC.getDigest(H);
		client = new SRP6ClientCustom();	
		client.init(N, g, digest, random);
	}
	
	public SrpClientPublicValues clientRound1(byte[] userId, byte[] password, byte[] salt,  BigInteger B) throws SrpException {
		BigInteger A ;
		BigInteger M1;
		
		A = client.generateClientCredentials(salt, userId, password);
		// Check B valid and calculate secret S
		try{
			client.calculateSecret(B); 
		} catch (CryptoException ce1) {
			throw new SrpException(ce1.getMessage(),SrpException.Type.INVALID_B_VALUE);
		}
		try{
			//calculate client evidence message M1
			M1 = client.calculateClientEvidenceMessage();
		} catch (CryptoException ce2) {
			throw new SrpException(ce2.getMessage(), SrpException.Type.INVALID_STATE);
		}
		
		return new SrpClientPublicValues(A,M1);
	}
	
	public BigInteger clientRound2( BigInteger M2) throws SrpException{
		BigInteger sessionKey;
		try{
			//Check client evidence message M1
			if (!client.verifyServerEvidenceMessage(M2))
				throw new SrpException(SrpException.Type.INVALID_CREDENTIALS);
			sessionKey= client.calculateSessionKey();
		} catch (CryptoException ce2) {
			throw new SrpException(ce2.getMessage(), SrpException.Type.INVALID_STATE);
		}
		return sessionKey;
	}

}

