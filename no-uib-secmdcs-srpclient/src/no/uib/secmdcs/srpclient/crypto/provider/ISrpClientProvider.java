package no.uib.secmdcs.srpclient.crypto.provider;
import java.math.BigInteger;
import no.uib.secmdcs.srpclient.crypto.SrpClientPublicValues;
import no.uib.secmdcs.srpclient.crypto.SrpException;

/**
 * Interface for SRP providers.
 * Unlike the server-side ones, client srp providers can be stateful.
 * @author daniele
 *
 */
public interface ISrpClientProvider {

	public SrpClientPublicValues clientRound1(byte[] userId, byte[] password, byte[] salt,  BigInteger B) throws SrpException;
	
	public BigInteger clientRound2(BigInteger M2) throws SrpException;

}