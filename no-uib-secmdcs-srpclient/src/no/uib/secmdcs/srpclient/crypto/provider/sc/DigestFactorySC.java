package no.uib.secmdcs.srpclient.crypto.provider.sc;

import org.spongycastle.crypto.Digest;
import org.spongycastle.crypto.digests.SHA1Digest;
import org.spongycastle.crypto.digests.SHA224Digest;
import org.spongycastle.crypto.digests.SHA256Digest;
import org.spongycastle.crypto.digests.SHA384Digest;
import org.spongycastle.crypto.digests.SHA512Digest;

/**
 * Class needed to create a Digest from the algorithm name.
 * The class also checks that the chosen algorithm is among 
 * the ones supported by this provider, and suitable for SRP.
 * @author daniele
 */
public class DigestFactorySC {
	
	public static Digest getDigest(String digestName) {
		Digest digest = null;
		if (digestName.equalsIgnoreCase("SHA-1"))
			digest = new SHA1Digest();
		else if (digestName.equalsIgnoreCase("SHA-224"))
			digest = new SHA224Digest();
		else if (digestName.equalsIgnoreCase("SHA-256"))
			digest = new SHA256Digest();
		else if (digestName.equalsIgnoreCase("SHA-384"))
			digest = new SHA384Digest();
		else if (digestName.equalsIgnoreCase("SHA-512"))
			digest = new SHA512Digest();
		else 
			throw new RuntimeException(	"The specified Digest algorithm '" + digestName 
									+ "' is not supported.");
		return digest;
	}
}
