package no.uib.secmdcs.srpclient.crypto;

import no.uib.secmdcs.srpclient.crypto.provider.ISrpClientProvider;

/**
 * A simple factory for the Srp Provider, based on a configuration property.
 * @author daniele
 *
 */
public class SrpClientProviderFactory {
	/* This factory is implemented with a static factory method rather than using a singleton, 
	 * since SrpService is stateless
	 */
	public static ISrpClientProvider getSrpClientProvider() {
	try {
		return SrpConst.SRP_PROVIDER_IMPL.newInstance();
	} catch (InstantiationException e){
		throw new RuntimeException("Error while creating the SRP provider instance "
				+"from the specified class '" + SrpConst.SRP_PROVIDER_IMPL.getSimpleName() + "'");
	} catch(IllegalAccessException e) {
		throw new RuntimeException("Error while creating the SRP provider instance "
				+"from the specified class '" + SrpConst.SRP_PROVIDER_IMPL.getSimpleName() + "'");
	}
}
}
