package no.uib.secmdcs.srpclient;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SrpClientConfig {
//TODO: move this in a config file
	//	private static final String SRP_SERVER_HOST = "http://no-uib-secmdcs-srpserver.appspot.com/";
	//private static final String SRP_SERVER_HOST = "http://10.0.2.2:8888/"; //to test it on local machine with emulator
	private static final String SRP_SERVER_ROUND1_PATH = "login1"; //the servlet mapping url for round 1
	private static final String SRP_SERVER_ROUND2_PATH = "login2"; //the servlet mapping url for round 2
	private static final String SRP_SERVER_PINNING_PATH = "pinning";
	
	public static String getSrpServerHost(Context ctx){
		// return "http://no-uib-secmdcs-srpserver.appspot.com/"; //to test it on a GAE instance
		// return "http://10.0.2.2:8888/"; //to test it on local machine with emulator
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		String host=prefs.getString("pref_srp_host", "http://");
		if (!host.contains("//")) host= "http://" + host;
		if (!host.endsWith("/")) host= host+"/";
		return host;
	}
	public static final String getSrpServerRound1Uri(String host){
		return host + SRP_SERVER_ROUND1_PATH;
	}
	public static final String getSrpServerRound2Uri(String host){
		return host + SRP_SERVER_ROUND2_PATH;
	}
	
	public static final String getSrpServerPinningUri(String host){
		return host + SRP_SERVER_PINNING_PATH;
	}
}