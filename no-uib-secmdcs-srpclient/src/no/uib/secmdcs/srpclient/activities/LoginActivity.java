package no.uib.secmdcs.srpclient.activities;

import java.math.BigInteger;

import org.apache.http.impl.client.DefaultHttpClient;

import no.uib.secmdcs.srpclient.CertificatePinner;
import no.uib.secmdcs.srpclient.R;
import no.uib.secmdcs.srpclient.SrpAuthenticator;
import no.uib.secmdcs.srpclient.SrpClientConfig;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Activity which displays a login screen to the user.
 */
public class LoginActivity extends Activity {

	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;
	
	
	private BigInteger sessKey = null; //the session key generated after SRP authentication.
	
	public String errorMsg; //error message to return if login is not successful
	
	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mPassword;

	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);
		setupActionBar();

		// Set up the login form.
		mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		mEmailView = (EditText) findViewById(R.id.email);
		mEmailView.setText(mEmail);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
		.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id,
					KeyEvent keyEvent) {
				if (id == R.id.login || id == EditorInfo.IME_NULL) {
					attemptLogin();
					return true;
				}
				return false;
			}
		});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// Show the Up button in the action bar.
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	/**
	 * Attempts to sign in the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} 
//		else if (mPassword.length() < 4) {
//			mPasswordView.setError(getString(R.string.error_invalid_password));
//			focusView = mPasswordView;
//			cancel = true;
//		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} 
//		else if (!mEmail.contains("@")) {
//			mEmailView.setError(getString(R.string.error_invalid_email));
//			focusView = mEmailView;
//			cancel = true;
//		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute();
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
			.alpha(show ? 1 : 0)
			.setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginStatusView.setVisibility(show ? View.VISIBLE
							: View.GONE);
				}
			});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
			.alpha(show ? 0 : 1)
			.setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginFormView.setVisibility(show ? View.GONE
							: View.VISIBLE);
				}
			});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	
	
	
	/**
	 * Represents an asynchronous login task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, String, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			String host = SrpClientConfig.getSrpServerHost(getApplicationContext());
			
			/* DefaultHttpClient:
			 * -	leaves the session open (handling session cookies) allowing 
			 * 		to save the SRP context on the server accross the roundtrips.
			 * -	Cannot be closed using httpclient.close()
			 */
			DefaultHttpClient httpClient = new DefaultHttpClient();
//			/* AndroidHttpClient: (alternative)
//			 * -	doesn't handle automatically the session cookies, so the session
//			 * 		data is not kept accross the two roundtrips.
//			 * -	Must be closed after use with httpclient.close() (finally)
//			 * -	Must specify a UserAgent on creation.
//			 */
//			String userAgent = "no.uib.secmdcs.srpclient";
//			HttpClient httpclient = AndroidHttpClient.newInstance(userAgent);
			
			
			/*	To handle the progress updates during authentication, 
			 *	we use an anonymous class that extends SrpAuthenticator,
			 *	providing local handlers for the exposed events
			 */
			SrpAuthenticator authenticator = new SrpAuthenticator(host, httpClient){
				@Override
				public void onBeginRound1(){
					publishProgress("SRP round 1...");
				}
				@Override
				public void onBeginRound2() {
					publishProgress("SRP round 2...");
				}
				@Override
				public void onSRPComplete() {
					publishProgress("SRP complete.");
				}
				@Override
				public void onError( String message) {
					publishProgress("Error occurred...");
					errorMsg = message;
				}
			};
			sessKey = authenticator.getAuthKey(mEmail, mPassword);
			
			if (sessKey!=null) {
			/*
				CertificatePinner certPinner = new CertificatePinner(httpClient){
					@Override
					public void onBeginCertificateRequest(){
						publishProgress("Downloading certificate...");
					}
					@Override
					public void onPinningStarted() {
						publishProgress("Pinning started...");
					}
					@Override
					public void onPinningComplete() {
						publishProgress("Pinning complete.");
					}
					@Override
					public void onError( String message) {
						publishProgress("Error occurred...");
						errorMsg = message;
					}
				};
				return certPinner.pinServerCertificate( sessKey, getApplicationContext(), host);
			*/
				return true;
				
			} else {
			return false;
			}
		
		}

		@Override
		protected void onProgressUpdate(String... progress) {
			mLoginStatusMessageView.setText(progress[0]);
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			Intent loginResult = new Intent(getApplicationContext(),LoginResultActivity.class     );                          
			loginResult.putExtra("no.uib.secmdcs.srpclient.login.success", success);
			if(success){
				loginResult.putExtra("no.uib.secmdcs.srpclient.login.user", mEmail );
				loginResult.putExtra("no.uib.secmdcs.srpclient.login.key", sessKey ); //remove in production!
			} else {
				loginResult.putExtra("no.uib.secmdcs.srpclient.login.error", errorMsg );
			}
			startActivity(loginResult);
			finish();
			showProgress(false);
			
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}

	}
}
