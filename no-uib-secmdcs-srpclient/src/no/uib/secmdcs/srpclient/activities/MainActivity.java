package no.uib.secmdcs.srpclient.activities;

import no.uib.secmdcs.srpclient.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
//import com.newrelic.agent.android.NewRelic;
import android.view.View;


public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		PreferenceManager.setDefaultValues(this, R.xml.preference, false);
		setContentView(R.layout.activity_main);
		/*NewRelic.withApplicationToken(
				"AAf43f3e9f22776b54f014450a4544dd5ce9989619"
				).start(this.getApplication());*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void goLogin(View view) {
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        default:
            return false;
        }
    }
	
/*	public void goTestChannel(View view) {
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);	}*/
}
