package no.uib.secmdcs.srpclient.activities;

import java.math.BigInteger;

import no.uib.secmdcs.srpclient.R;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

public class LoginResultActivity extends Activity {

	private ImageView mLoginResultImageView;
	private TextView mLoginResultMessageView;
	private TextView mLoginErrorMessageView;
	private TextView mLoggedUserView;
	private TextView mSessionInfoView; //TODO: only for test

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_result);
		// Show the Up button in the action bar.
		setupActionBar();
		mLoginResultImageView= (ImageView) findViewById(R.id.login_result_image);
		mLoginResultMessageView = (TextView) findViewById(R.id.login_result_message);
		mLoginErrorMessageView = (TextView) findViewById(R.id.login_error_message);
		mLoggedUserView = (TextView) findViewById(R.id.logged_user);
		mSessionInfoView = (TextView) findViewById(R.id.session_info); //TODO: only for test
		
		Intent myIntent= getIntent(); // gets the previously created intent
		boolean success = myIntent.getBooleanExtra("no.uib.secmdcs.srpclient.login.success", false); 
		
		if (success){
			mLoginResultImageView.setImageResource(R.drawable.icon_button_ok);
			mLoginResultMessageView.setTextColor(Color.DKGRAY);
			mLoginResultMessageView.setText("Login Successful.");
			mLoginErrorMessageView.setVisibility(TextView.INVISIBLE);
			String userID = myIntent.getStringExtra("no.uib.secmdcs.srpclient.login.user");
			mLoggedUserView.setText("User:  " + userID);
			mLoggedUserView.setVisibility(TextView.VISIBLE);
			
			//TODO: only for test
			BigInteger sessKey= (BigInteger) myIntent.getSerializableExtra("no.uib.secmdcs.srpclient.login.key");
			mSessionInfoView.setText("Session Key \n \n" + sessKey);
			mSessionInfoView.setVisibility(TextView.VISIBLE);
		} else {
			
			mLoginResultImageView.setImageResource(R.drawable.icon_button_cancel);
			
			String errorMsg = myIntent.getStringExtra("no.uib.secmdcs.srpclient.login.error");
			if (errorMsg == null){
				mLoginResultMessageView.setTextColor(Color.RED);
				mLoginResultMessageView.setText("Login Failed.");
			}else{
				mLoginResultMessageView.setTextColor(Color.DKGRAY);
				mLoginResultMessageView.setText("Error!");
				
				mLoginErrorMessageView.setText(errorMsg);
				mLoginErrorMessageView.setVisibility(TextView.VISIBLE);
			}
			mLoggedUserView.setVisibility(TextView.INVISIBLE);
			mSessionInfoView.setVisibility(TextView.INVISIBLE); //TODO: only for test
		}

		
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login_result, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
