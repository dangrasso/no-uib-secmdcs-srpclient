package no.uib.secmdcs.srpclient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

import no.uib.secmdcs.srpclient.crypto.SrpClientProviderFactory;
import no.uib.secmdcs.srpclient.crypto.provider.ISrpClientProvider;
import no.uib.secmdcs.srpclient.crypto.SrpClientPublicValues;
import no.uib.secmdcs.srpclient.crypto.SrpException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;



//import android.net.http.AndroidHttpClient;
import android.util.Base64;
import android.util.Log;

public class SrpAuthenticator {
	private HttpClient httpClient;
	private String host;
	
	public SrpAuthenticator( String host, DefaultHttpClient httpclient) {
		super();
		this.host=host;
		this.httpClient = httpclient;
	}

	public SrpAuthenticator( String host) {
		super();
		this.host=host;
		this.httpClient = new DefaultHttpClient();
	}
	
	/**
	 * Perform Secure Remote Password mutual authentication and return the generated session key.
	 * @param userId
	 * @param password
	 * @return Key: the symmetric session key generated during SRP authentication
	 */
	public final BigInteger getAuthKey(String userId, String password)  {
		BigInteger sessKey = null;
		String log_tag = "SrpAuthenticator";
		
		/*
		 * TODO: provide an alternative implementation using the recommended HTTPUrlConnection
		 */
		
		//		try {
		ISrpClientProvider srp = SrpClientProviderFactory.getSrpClientProvider();

		//============================ ROUND 1 ============================
		onBeginRound1(); //Raise Event
		
		//SRP Round1 request [--->UserID(I)]
		HttpPost reqRound1 = new HttpPost(SrpClientConfig.getSrpServerRound1Uri(host));

		reqRound1.setHeader("Accept", "application/json");
		reqRound1.setHeader("Content-type", "application/json");
		JSONObject jsonReq1 = new JSONObject();
		try {
			jsonReq1.put("SRP_I", userId);
			Log.i(log_tag, "Round 1 json request: " + jsonReq1.toString(4));
			reqRound1.setEntity(new StringEntity(jsonReq1.toString()));
			Log.i(log_tag, "Round 1 request ready");
		} catch (JSONException e) {
			String message = "Error while writing client parameters to JSON for round 1 request";
			Log.e(log_tag, message, e);
			onError(message);
			return null;
		} catch (UnsupportedEncodingException e) {
			String message = "Error while encoding client parameters for round 1 request";
			Log.e(log_tag, message, e);
			onError(message);
			return null;
		}

		//SRP Round1 response [salt(S),B<---]
		HttpResponse resRound1;
		try {
			resRound1 = httpClient.execute(reqRound1);
			Log.i(log_tag, "Request sent");
			Log.i(log_tag, "Received round 1 response from server");
		} catch (IOException e) {
			String message = "HttpClient failure in SRP round 1: check the hostname of the server. The current hostname is: " + host;
			Log.e(log_tag, message + e.getMessage(), e);
			onError(message);
			return null;
		}

		//TODO:check if redirected

		//check status code
		if (resRound1.getStatusLine().getStatusCode()==HttpStatus.SC_OK){
			Log.i(log_tag, "Received response OK from server for round 1.");
		}else {	
			String message = "Unexpected response from server for round 1: " 
					+ resRound1.getStatusLine().getStatusCode() + ": "
					+ resRound1.getStatusLine().getReasonPhrase();
			Log.e(log_tag, message);
			onError(message);
			return null;
		}
		//and if entity exists
		HttpEntity resRound1Entity = resRound1.getEntity();
		if (resRound1Entity==null){
			String message = "Missing entity in round 1 server response";
			Log.e(log_tag, message);
			onError(message);
			return null;
		}
		//read the json object
		JSONObject jsonRes1;
		try {
			jsonRes1 = new JSONObject(EntityUtils.toString(resRound1Entity));
			Log.i(log_tag, "Round 1 json response: " + jsonRes1.toString(4));
		} catch (IOException e) {
			String message = "Error while processing round 1 response entity";
			Log.e(log_tag, message, e);
			onError(message);
			return null;
		} catch (JSONException e) {
			String message = "Error while parsing server parameters to JSON for round 1 response";
			Log.e(log_tag, message, e);
			onError(message);
			return null;
		}

		//check if it contains the needed parameters
		if (!jsonRes1.has("SRP_S")){
			String message = "Missing parameter 'SRP_S' in round 1 server response";
			Log.e(log_tag, message);
			onError(message);
			return null;
		}
		if (!jsonRes1.has("SRP_B")){
			String message = "Missing parameter 'SRP_B' in round 1 server response";
			Log.e(log_tag, message);
			onError(message);
			return null;
		}
		//extract the parameters
		byte[] rec_salt;
		BigInteger rec_B;

		try{
			rec_salt = Base64.decode(jsonRes1.getString("SRP_S"),Base64.DEFAULT); //the salt is decoded from its Base64 representation
			// Log.i(log_tag, "Round 1 server value SRP_S (byte[]) : " + Arrays.toString(rec_salt));
			rec_B = new BigInteger(jsonRes1.getString("SRP_B"));
			// Log.i(log_tag, "Round 1 server value SRP_B (BigInteger) : " + rec_B.toString());
		} catch (JSONException e) {
			String message = "Error while parsing server parameters from JSON for round 1 response";
			Log.e(log_tag, message, e);
			onError(message);
			return null;
		}

		//Complete Round1 and calculate values for Round2
		SrpClientPublicValues clientPubVals;
		try {	
			clientPubVals = srp.clientRound1(userId.getBytes(),
					password.getBytes(), 
					rec_salt, 
					rec_B);
		} catch (SrpException e) {
			String message = "SRP client round 1 failure";
			Log.e(log_tag, message, e);
			onError(message);
			return null;
		}


		String _A = clientPubVals.getA().toString();
		String _M1 = clientPubVals.getM1().toString();

		//============================ ROUND 2 ============================
		onBeginRound2(); //Raise Event
		
		//SRP Round2 request [--->A,M1]
		HttpPost reqRound2 = new HttpPost(SrpClientConfig.getSrpServerRound2Uri(host));

		reqRound2.setHeader("Accept", "application/json");
		reqRound2.setHeader("Content-type", "application/json");
		JSONObject jsonReq2 = new JSONObject();
		try {
			jsonReq2.put("SRP_A", _A);
			jsonReq2.put("SRP_M1", _M1);
			Log.i(log_tag, "Round 2 json request: " + jsonReq2.toString(4));
			reqRound2.setEntity(new StringEntity(jsonReq2.toString()));
			Log.i(log_tag, "Round 2 request ready");
		} catch (JSONException e) {
			String message = "Error while writing client parameters to JSON for round 2 request";
			Log.e(log_tag, message, e);
			onError(message);
			return null;
		} catch (UnsupportedEncodingException e) {
			String message = "Error while encoding client parameters for round 2 request";
			Log.e(log_tag, message, e);
			onError(message);
			return null;
		}

		//SRP Round2 response [M2<---]
		HttpResponse resRound2;
		try {
			resRound2 = httpClient.execute(reqRound2);
			Log.i(log_tag, "Request sent");
		} catch (IOException e) {
			String message = "HttpClient failure in SRP round 2";
			Log.e(log_tag, message, e);
			onError(message);
			return null;
		}

		//TODO:check if redirected (?)

		//check status code
		if (resRound2.getStatusLine().getStatusCode()==HttpStatus.SC_OK){
			Log.i(log_tag, "Received response OK from server for round 2. (Client Authenticated)");
		}else if (resRound2.getStatusLine().getStatusCode()==HttpStatus.SC_UNAUTHORIZED) {
			Log.i(log_tag, "Received response UNAUTHORIZED from server for round 2. (Client not authenticated)");
			return null;
		}else {	
			String message = "Unexpected response from server for round 2: " 
					+ resRound2.getStatusLine().getStatusCode() + ": "
					+ resRound2.getStatusLine().getReasonPhrase();
			Log.e(log_tag, message);
			onError(message);
			return null;
		}
		//and if entity exists
		HttpEntity resRound2Entity = resRound2.getEntity();
		if (resRound2Entity==null){
			Log.e(log_tag, "Missing entity in round 2 server response");
			return null;
		}
		//read the json object
		JSONObject jsonRes2;
		try {
			jsonRes2 = new JSONObject(EntityUtils.toString(resRound2Entity));
			Log.i(log_tag, "Round 2 json response: " + jsonRes2.toString(4));
		} catch (IOException e) {
			String message = "Error while processing round 2 response entity";
			Log.e(log_tag, message, e);
			onError(message);
			return null;
		} catch (JSONException e) {
			String message = "Error while parsing server parameters to JSON for round 2 response";
			Log.e(log_tag,message, e);
			onError(message);
			return null;
		}
		//check if it contains the needed parameters
		if (!jsonRes2.has("SRP_M2")){
			String message = "Missing parameter 'SRP_M2' in round 2 server response";
			Log.e(log_tag, message);
			onError(message);
			return null;
		}
		//extract the parameters
		BigInteger rec_M2;
		try{
			rec_M2 = new BigInteger(jsonRes2.getString("SRP_M2"));
			Log.i(log_tag, "Round 2 server value SRP_M2 (BigInteger) : " + rec_M2.toString());
		} catch (JSONException e) {
			String message = "Error while parsing server parameters from JSON for round 2 response";
			Log.e(log_tag, message, e);
			onError(message);
			return null;
		}

		//Verify server evidence message and calculate the session Key
		try {
			sessKey = srp.clientRound2(rec_M2);
		} catch (SrpException e) {
			String message = "SRP client round 2 failure. (Server not authenticated)";
			Log.e(log_tag, message, e);
			onError(message);
		}
		
		onSRPComplete(); //Raise Event
		
		
		Log.i(log_tag, "(Server authenticated)");
		Log.i(log_tag, "SRP Session Key created.");

		//TODO: REMOVE THIS IN PRODUCTION
		Log.i(log_tag, "***** sessKey: " + sessKey.toString()); 


		return sessKey;

	}
	
	// events that can optionally be handled
	public void onBeginRound1(){}
	public void onBeginRound2(){}
	public void onSRPComplete(){}
	public void onError(String errorMsg){}
}
